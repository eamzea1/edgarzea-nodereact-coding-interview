import React, { FC, useState, useEffect, ChangeEvent } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import FilterInput from "../components/filter/filter-input";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [filtering, setFiltering] = useState('');

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    handleFilter()
  }, [filtering])

  const fetchData = async () => {
    try {
      const result = await backendClient.getAllUsers();
      setUsers(result.data);
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
    }
  };

  const handleChange = ({target: {value}} : ChangeEvent<HTMLInputElement>) => {
    setFiltering(value)
  }

  const handleFilter = () => {
    const filtered = users.filter(user => [user.company, user.email, user.first_name, user.gender, user.last_name, user.title].includes(filtering));


  }


  return (
    <div style={{ paddingTop: "30px" }}>
      <FilterInput value={filtering} handler={handleChange} />
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {isLoading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
