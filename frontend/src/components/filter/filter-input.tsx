import { TextField } from '@mui/material'
import React, { ChangeEvent, Dispatch, SetStateAction } from 'react'

interface FilterInputProps {
  value: string
  handler: ({target: {value}} : ChangeEvent<HTMLInputElement>) => void
}

const FilterInput:React.FC<FilterInputProps> = ({value, handler}) => {
  return (
    <div style={{ margin: "50px 0" }}>
      <TextField id="outlined-basic" label="Outlined" variant="outlined" value={value} onChange={handler}/>
    </div>
  )
}

export default FilterInput
